# Welcomm to the Jerry's Folder

<center><img src="https://user-images.githubusercontent.com/54479819/64472272-995fc200-d196-11e9-8e2e-39001540da44.jpg" width="400" hight="400"></center>

Hello. This is Jerry.<br>
I am an **_embedded system engineer_** and deal with **_Firmware_**, **_BSP_** based on Micom or Linux.<br>

This folder is where ***BeagleBone Black's device driver code*** is organized.<br>
If you have questions, anyway feedback to me.<br>
Thanks :)

Click my blog	 : **_[Jerry's blog](https://blog.naver.com/snim008)_**<br>
Click my resume	: **_[LinkedIn](https://www.linkedin.com/in/leejaeseong-19871224)_**
