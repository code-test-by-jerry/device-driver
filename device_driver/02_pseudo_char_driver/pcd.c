#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <linux/module.h>
#include <linux/uaccess.h>

#define DEV_MEM_SIZE 512

#ifdef pr_fmt
#undef pr_fmt
#define pr_fmt(fmt) "%s:" fmt, __func__
#endif

static int _pcd_open(struct inode *inode, struct file *filp);
static int _pcd_release(struct inode *inode, struct file *filp);
static ssize_t _pcd_read(struct file *filp, char __user *buff, size_t count,
                         loff_t *f_pos);
static ssize_t _pcd_write(struct file *filp, const char __user *buff,
                          size_t count, loff_t *f_pos);
static loff_t _pcd_lseek(struct file *filp, loff_t offset, int whence);

static inline void _pseudo_driver_exit(void);

dev_t dev_num;
char dev_buf[DEV_MEM_SIZE];
struct cdev pcd_cdev;
struct class *class_pcd;
struct device *device_pcd;
struct file_operations pcd_fops = {.open = _pcd_open,
                                   .release = _pcd_release,
                                   .read = _pcd_read,
                                   .write = _pcd_write,
                                   .llseek = _pcd_lseek,
                                   .owner = THIS_MODULE};

static int __init _pseudo_driver_init(void)
{
    int ret = -EPERM;

    /* Dynamically allocate a device number */
    ret = alloc_chrdev_region(&dev_num, 0, 1, "pcd_deivces");
    if (ret < 0)
        return _pseudo_driver_exit(), ret;

    pr_info("dev num <%d>:<%d>\n", MAJOR(dev_num), MINOR(dev_num));

    /* Initialize the cdev structure with fops */
    cdev_init(&pcd_cdev, &pcd_fops);
    pcd_cdev.owner = THIS_MODULE;

    /* Register a device(cdev struct) with VFS */
    ret = cdev_add(&pcd_cdev, dev_num, 1);
    if (ret < 0)
        return _pseudo_driver_exit(), ret;

    /* create device class under /sys/class */
    class_pcd = class_create(THIS_MODULE, "pcd_class");
    if (IS_ERR(class_pcd))
        return _pseudo_driver_exit(), PTR_ERR(class_pcd);

    /* populate the sysfs with device information */
    device_pcd = device_create(class_pcd, NULL, dev_num, NULL, "pcd");
    if (IS_ERR(device_pcd))
        return _pseudo_driver_exit(), PTR_ERR(device_pcd);

    pr_info("Success the module registeration!");
    return 0;
}

static void __exit _pseudo_driver_cleanup(void)
{
    _pseudo_driver_exit();
    pr_info("module unloaded!\n");
}

static int _pcd_open(struct inode *inode, struct file *filp)
{
    pr_info("Success to open the pcd.\n");
    return 0;
}

static int _pcd_release(struct inode *inode, struct file *filp)
{
    pr_info("Success to close the pcd.\n");
    return 0;
}

static ssize_t _pcd_read(struct file *filp, char __user *buff, size_t count,
                         loff_t *f_pos)
{
    int ret = -EPERM;

    pr_info("read requested for %zu bytes\n", count);
    pr_info("current file position = %lld\n", *f_pos);

    /* Adjust the 'count' */
    if ((*f_pos + count) > DEV_MEM_SIZE)
        count = DEV_MEM_SIZE - *f_pos;

    /* copy to user */
    ret = copy_to_user(buff, &dev_buf[*f_pos], count);
    if (ret < 0)
        return -EFAULT;

    /* update the current file position */
    *f_pos += count;

    pr_info("Number of bytes successfully read = %zu\n", count);
    pr_info("Updated file position = %lld\n", *f_pos);

    /* return number of bytes which have successfully read */
    return count;
}

static ssize_t _pcd_write(struct file *filp, const char __user *buff,
                          size_t count, loff_t *f_pos)
{
    int ret = -EPERM;

    pr_info("write requested for %zu bytes\n", count);
    pr_info("current file position = %lld\n", *f_pos);

    /* Adjust the 'count' */
    if ((*f_pos + count) > DEV_MEM_SIZE)
        count = DEV_MEM_SIZE - *f_pos;

    if (!count)
    {
        pr_err("No space left on the device!\n");
        return -ENOMEM;
    }

    /* copy from user */
    ret = copy_from_user(&dev_buf[*f_pos], buff, count);
    if (ret < 0)
        return -EFAULT;

    /* update the current file position */
    *f_pos += count;

    pr_info("Number of bytes successfully write = %zu\n", count);
    pr_info("Updated file position = %lld\n", *f_pos);

    return count;
}

static loff_t _pcd_lseek(struct file *filp, loff_t offset, int whence)
{
    loff_t predict;
    pr_info("lseek requested\n");
    pr_info("current valut of the file position = %lld\n", filp->f_pos);

    switch (whence)
    {
    case SEEK_SET:
        if (offset > DEV_MEM_SIZE || offset < 0)
            return -EINVAL;

        filp->f_pos = offset;
        break;

    case SEEK_CUR:
        predict = filp->f_pos + offset;
        if (predict > DEV_MEM_SIZE || predict < 0)
            return -EINVAL;

        filp->f_pos = predict;
        break;

    case SEEK_END:
        predict = DEV_MEM_SIZE + offset;
        if (predict > DEV_MEM_SIZE || predict < 0)
            return -EINVAL;

        filp->f_pos = predict;
        break;

    default:
        return -EINVAL;
    }

    pr_info("New value of the file position = %lld\n", filp->f_pos);
    return filp->f_pos;
}

static inline void _pseudo_driver_exit(void)
{
    if (dev_num)
    {
        if (class_pcd)
        {
            if (device_pcd)
            {
                device_destroy(class_pcd, dev_num);
                device_pcd = NULL;
            }
            class_destroy(class_pcd);
            class_pcd = NULL;
        }
        cdev_del(&pcd_cdev);
        memset(&pcd_cdev, 0x0, sizeof(struct cdev));

        unregister_chrdev_region(dev_num, 1);
        dev_num = 0;
    }
}

module_init(_pseudo_driver_init);
module_exit(_pseudo_driver_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Lee, Jerry J");
MODULE_DESCRIPTION("A simple pseudo char driver module");
MODULE_INFO(board, "Beaglebone black REV A5");
