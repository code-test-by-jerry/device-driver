#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <linux/module.h>
#include <linux/uaccess.h>

#define MEM_SIZE_MAX_PCDDEV1 1024
#define MEM_SIZE_MAX_PCDDEV2 512
#define MEM_SIZE_MAX_PCDDEV3 1024
#define MEM_SIZE_MAX_PCDDEV4 512

#define NO_OF_DEVICES 4

#define RDONLY 0x01
#define WRONLY 0x10
#define RDWR (RDONLY | WRONLY)

#ifdef pr_fmt
#undef pr_fmt
#define pr_fmt(fmt) "%s:" fmt, __func__
#endif

static int __init _pseudo_driver_init(void);
static void __exit _pseudo_driver_cleanup(void);

static int _pcd_open(struct inode *inode, struct file *filp);
static int _pcd_release(struct inode *inode, struct file *filp);
static ssize_t _pcd_read(struct file *filp, char __user *buff, size_t count,
                         loff_t *f_pos);
static ssize_t _pcd_write(struct file *filp, const char __user *buff,
                          size_t count, loff_t *f_pos);
static loff_t _pcd_lseek(struct file *filp, loff_t offset, int whence);

static inline int _check_permission(int dev_perm, int acc_mode);
static inline void _pseudo_driver_exit(void);

/* Device private data structure */
struct pcdev_private_data
{
    int perm;
    unsigned int size;

    char *buffer;
    const char *serial_number;

    struct cdev cdev;
};

/* Driver private data structure */
struct pcdrv_private_data
{
    dev_t dev_num;

    struct class *class_pcd;
    struct device *device_pcd[NO_OF_DEVICES];
    struct pcdev_private_data pcdev_data[NO_OF_DEVICES];
};

char dev_buf_pcdev1[MEM_SIZE_MAX_PCDDEV1];
char dev_buf_pcdev2[MEM_SIZE_MAX_PCDDEV2];
char dev_buf_pcdev3[MEM_SIZE_MAX_PCDDEV3];
char dev_buf_pcdev4[MEM_SIZE_MAX_PCDDEV4];

struct pcdrv_private_data pcdrv_data = {
    .pcdev_data = {[0] =
                       {
                           .perm = RDONLY, /* readonly */
                           .size = MEM_SIZE_MAX_PCDDEV1,
                           .buffer = dev_buf_pcdev1,
                           .serial_number = "PCDEV1XYZ123",
                       },
                   [1] =
                       {
                           .perm = WRONLY, /* writeonly */
                           .size = MEM_SIZE_MAX_PCDDEV2,
                           .buffer = dev_buf_pcdev2,
                           .serial_number = "PCDEV1XYZ123",
                       },
                   [2] =
                       {
                           .perm = RDWR, /* rw */
                           .size = MEM_SIZE_MAX_PCDDEV3,
                           .buffer = dev_buf_pcdev3,
                           .serial_number = "PCDEV1XYZ123",
                       },
                   [3] = {
                       .perm = RDWR, /* rw */
                       .size = MEM_SIZE_MAX_PCDDEV4,
                       .buffer = dev_buf_pcdev4,
                       .serial_number = "PCDEV1XYZ123",
                   }}};

struct file_operations pcd_fops = {.open = _pcd_open,
                                   .release = _pcd_release,
                                   .read = _pcd_read,
                                   .write = _pcd_write,
                                   .llseek = _pcd_lseek,
                                   .owner = THIS_MODULE};

static int __init _pseudo_driver_init(void)
{
    int i;
    int ret = -EPERM;

    /* Dynamically allocate a device number */
    ret = alloc_chrdev_region(&pcdrv_data.dev_num, 0, NO_OF_DEVICES,
                              "pcd_deivces");
    if (ret < 0)
        return _pseudo_driver_exit(), ret;

    /* create device class under /sys/class */
    pcdrv_data.class_pcd = class_create(THIS_MODULE, "pcd_class");
    if (IS_ERR(pcdrv_data.class_pcd))
        return _pseudo_driver_exit(), PTR_ERR(pcdrv_data.class_pcd);

    for (i = 0; i < NO_OF_DEVICES; i++)
    {
        pr_info("dev num <%d>:<%d>\n", MAJOR(pcdrv_data.dev_num + i),
                MINOR(pcdrv_data.dev_num + i));

        /* Initialize the cdev structure with fops */
        cdev_init(&pcdrv_data.pcdev_data[i].cdev, &pcd_fops);
        pcdrv_data.pcdev_data[i].cdev.owner = THIS_MODULE;

        /* Register a device(cdev struct) with VFS */
        ret =
            cdev_add(&pcdrv_data.pcdev_data[i].cdev, pcdrv_data.dev_num + i, 1);
        if (ret < 0)
            return _pseudo_driver_exit(), ret;

        /* populate the sysfs with device information */
        pcdrv_data.device_pcd[i] =
            device_create(pcdrv_data.class_pcd, NULL, pcdrv_data.dev_num + i,
                          NULL, "pcdev-%d", i + 1);
        if (IS_ERR(pcdrv_data.device_pcd[i]))
            return _pseudo_driver_exit(), PTR_ERR(pcdrv_data.device_pcd[i]);
    }

    pr_info("Success the module registeration!");
    return 0;
}

static void __exit _pseudo_driver_cleanup(void)
{
    _pseudo_driver_exit();
    pr_info("Module unloaded!\n");
}

static int _pcd_open(struct inode *inode, struct file *filp)
{
    int ret;
    int minor_n;
    struct pcdev_private_data *pcdev_data;

    /* find out on which device file open by user space*/
    minor_n = MINOR(inode->i_rdev);
    pr_info("minor access = %d\n", minor_n);

    /* get device's private data structure */
    pcdev_data = container_of(inode->i_cdev, struct pcdev_private_data, cdev);

    /* to supply device private data to other methods of the driver */
    filp->private_data = pcdev_data;

    /* check permission */
    ret = _check_permission(pcdev_data->perm, filp->f_mode);
    if (ret < 0)
    {
        pr_err("Failed to open!\n");
        return ret;
    }

    pr_info("Success to the open!\n");
    return 0;
}

static int _pcd_release(struct inode *inode, struct file *filp)
{
    pr_info("Success to close the pcd.\n");
    return 0;
}

static ssize_t _pcd_read(struct file *filp, char __user *buff, size_t count,
                         loff_t *f_pos)
{
    int ret = -EPERM;
    struct pcdev_private_data *pcdev_data =
        (struct pcdev_private_data *)filp->private_data;
    int max_size = pcdev_data->size;

    pr_info("read requested for %zu bytes\n", count);
    pr_info("current file position = %lld\n", *f_pos);

    /* Adjust the 'count' */
    if ((*f_pos + count) > max_size)
        count = max_size - *f_pos;

    /* copy to user */
    ret = copy_to_user(buff, &pcdev_data->buffer[*f_pos], count);
    if (ret < 0)
        return -EFAULT;

    /* update the current file position */
    *f_pos += count;

    pr_info("Number of bytes successfully read = %zu\n", count);
    pr_info("Updated file position = %lld\n", *f_pos);

    /* return number of bytes which have successfully read */
    return count;
}

static ssize_t _pcd_write(struct file *filp, const char __user *buff,
                          size_t count, loff_t *f_pos)
{
    struct pcdev_private_data *pcdev_data =
        (struct pcdev_private_data *)filp->private_data;
    int max_size = pcdev_data->size;
    int ret = -EPERM;

    pr_info("write requested for %zu bytes\n", count);
    pr_info("current file position = %lld\n", *f_pos);

    /* Adjust the 'count' */
    if ((*f_pos + count) > max_size)
        count = max_size - *f_pos;

    if (!count)
    {
        pr_err("No space left on the device!\n");
        return -ENOMEM;
    }

    /* copy from user */
    ret = copy_from_user(&pcdev_data->buffer[*f_pos], buff, count);
    if (ret < 0)
        return -EFAULT;

    /* update the current file position */
    *f_pos += count;

    pr_info("Number of bytes successfully write = %zu\n", count);
    pr_info("Updated file position = %lld\n", *f_pos);

    return count;
}

static loff_t _pcd_lseek(struct file *filp, loff_t offset, int whence)
{
    struct pcdev_private_data *pcdev_data =
        (struct pcdev_private_data *)filp->private_data;
    int max_size = pcdev_data->size;

    loff_t predict;
    pr_info("lseek requested\n");
    pr_info("current valut of the file position = %lld\n", filp->f_pos);

    switch (whence)
    {
    case SEEK_SET:
        if (offset > max_size || offset < 0)
            return -EINVAL;

        filp->f_pos = offset;
        break;

    case SEEK_CUR:
        predict = filp->f_pos + offset;
        if (predict > max_size || predict < 0)
            return -EINVAL;

        filp->f_pos = predict;
        break;

    case SEEK_END:
        predict = max_size + offset;
        if (predict > max_size || predict < 0)
            return -EINVAL;

        filp->f_pos = predict;
        break;

    default:
        return -EINVAL;
    }

    pr_info("New value of the file position = %lld\n", filp->f_pos);
    return filp->f_pos;
}

static inline int _check_permission(int dev_perm, int acc_mode)
{
    if (dev_perm == RDWR)
        return 0;

    /* ensures readonly access */
    if (dev_perm == RDONLY &&
        ((acc_mode & FMODE_READ) && !(acc_mode & FMODE_WRITE)))
        return 0;

    /* ensures writeonly access */
    if (dev_perm == WRONLY &&
        (!(acc_mode & FMODE_READ) && (acc_mode & FMODE_WRITE)))
        return 0;

    return -EPERM;
}

static inline void _pseudo_driver_exit(void)
{
    int i;

    if (pcdrv_data.dev_num)
    {
        if (pcdrv_data.class_pcd)
        {
            for (i = 0; i < NO_OF_DEVICES; i++)
            {
                if (pcdrv_data.device_pcd[i])
                    device_destroy(pcdrv_data.class_pcd,
                                   pcdrv_data.dev_num + i);

                cdev_del(&pcdrv_data.pcdev_data[i].cdev);
            }
            class_destroy(pcdrv_data.class_pcd);
        }
        unregister_chrdev_region(pcdrv_data.dev_num, NO_OF_DEVICES);
        memset(&pcdrv_data, 0x0, sizeof(struct pcdrv_private_data));
    }
}

module_init(_pseudo_driver_init);
module_exit(_pseudo_driver_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Lee, Jerry J");
MODULE_DESCRIPTION("A pseudo character driver which handles n devices.");
MODULE_INFO(board, "Beaglebone black REV A5");
